function saved(ctx, f) {
    ctx.save();
    f();
    ctx.restore();
}

class Eyes {
    constructor({width = 150, height = 100, pupil = 'green'}={}) {
        // In DOM space.
        this.width = width;
        this.height = height;
        this.pupil = pupil;

        this._canvas = document.createElement('canvas');
        this._canvas.className = 'eyes';
        document.body.appendChild(this._canvas);

        this._ctx = this._canvas.getContext('2d');
        var devicePixelRatio = window.devicePixelRatio || 1;
        var backingStoreRatio = this._ctx.webkitBackingStorePixelRatio ||
                                this._ctx.mozBackingStorePixelRatio ||
                                this._ctx.msBackingStorePixelRatio ||
                                this._ctx.oBackingStorePixelRatio ||
                                this._ctx.backingStorePixelRatio || 1;

        // Render at display DPI, but scale canvas coordinates to match
        // DOM space. This saves having to transform the mouse position
        // by hand.
        var ratio = devicePixelRatio / backingStoreRatio;
        this._canvas.width  = width  * ratio;
        this._canvas.height = height * ratio;
        this._ctx.scale(ratio, ratio);

        this._canvas.style.width = width + 'px';
        this._canvas.style.height = height + 'px';
    }

    _clear() {
        this._ctx.clearRect(0, 0, this.width, this.height);
    }

    _drawEye({x, y, width, height}, mouse) {
        var ctx = this._ctx;
        saved(ctx, () => {
            let translate = {
                x: x + width / 2,
                y: y + height / 2,
            };
            let scale = {
                x: (width - 10) / 2,
                y: (height - 10) / 2,
            };
            ctx.translate(translate.x, translate.y);
            ctx.scale(scale.x, scale.y);

            // Eyeball is a 2×2 circle centred at (0, 0)
            let lineWidth = 9 / Math.max(width, height);

            let mouse_ = {
                x: (mouse.x - (translate.x + this._canvas.offsetLeft)) / scale.x,
                y: (mouse.y - (translate.y + this._canvas.offsetTop )) / scale.y,
            };
            var r = Math.sqrt(Math.pow(mouse_.x, 2) + Math.pow(mouse_.y, 2));
            let radius = 0.5;
            if (r > radius) {
                let ratio = radius / r;
                mouse_.x *= ratio;
                mouse_.y *= ratio;
            }
            this._drawEyeTransformed(ctx, lineWidth, mouse_);
        });
    }

    _drawEyeTransformed(ctx, lineWidth, mouse) {
        // sclera
        ctx.beginPath();
        ctx.arc(0, 0, 1, 0, 2 * Math.PI);
        ctx.fillStyle = '#eeeeee';
        ctx.fill();

        // border
        ctx.beginPath();
        ctx.arc(0, 0, 1, 0, 2 * Math.PI);
        ctx.strokeStyle = 'black';
        ctx.lineWidth = lineWidth;
        ctx.stroke();

        // pupil
        ctx.beginPath();
        ctx.arc(mouse.x, mouse.y, lineWidth * 2, 0, 2 * Math.PI);
        ctx.fillStyle = this.pupil;
        ctx.fill();

        // iris
        ctx.beginPath();
        ctx.arc(mouse.x, mouse.y, lineWidth * 2, 0, 2 * Math.PI);
        ctx.strokeStyle = 'black';
        ctx.stroke();
    }

    draw(mouse = {x: 0, y: 0}) {
        this._clear();

        this._drawEye({
            x: 0,
            y: 0,
            width: this.width / 2,
            height: this.height,
        }, mouse);
        this._drawEye({
            x: this.width / 2,
            y: 0,
            width: this.width / 2,
            height: this.height,
        }, mouse);
    }
}

const colours = _.shuffle([
    'aliceblue',
    'antiquewhite',
    'aquamarine',
    'azure',
    'beige',
    'bisque',
    'blanchedalmond',
    'blueviolet',
    'brown',
    'burlywood',
    'cadetblue',
    'chartreuse',
    'chocolate',
    'coral',
    'cornflowerblue',
    'cornsilk',
    'crimson',
    'cyan',
    'darkblue',
    'darkcyan',
    'darkgoldenrod',
    'darkgray',
    'darkgreen',
    'darkgrey',
    'darkkhaki',
    'darkmagenta',
    'darkolivegreen',
    'darkorange',
    'darkorchid',
    'darkred',
    'darksalmon',
    'darkseagreen',
    'darkslateblue',
    'darkslategray',
    'darkslategrey',
    'darkturquoise',
    'darkviolet',
    'deeppink',
    'deepskyblue',
    'dimgray',
    'dimgrey',
    'dodgerblue',
    'firebrick',
    'floralwhite',
    'forestgreen',
    'gainsboro',
    'ghostwhite',
    'gold',
    'goldenrod',
    'greenyellow',
    'grey',
    'honeydew',
    'hotpink',
    'indianred',
    'indigo',
    'ivory',
    'khaki',
    'lavender',
    'lavenderblush',
    'lawngreen',
    'lemonchiffon',
    'lightblue',
    'lightcoral',
    'lightcyan',
    'lightgoldenrodyellow',
    'lightgray',
    'lightgreen',
    'lightgrey',
    'lightpink',
    'lightsalmon',
    'lightseagreen',
    'lightskyblue',
    'lightslategray',
    'lightslategrey',
    'lightsteelblue',
    'lightyellow',
    'limegreen',
    'linen',
    'mediumaquamarine',
    'mediumblue',
    'mediumorchid',
    'mediumpurple',
    'mediumseagreen',
    'mediumslateblue',
    'mediumspringgreen',
    'mediumturquoise',
    'mediumvioletred',
    'midnightblue',
    'mintcream',
    'mistyrose',
    'moccasin',
    'navajowhite',
    'oldlace',
    'olivedrab',
    'orangered',
    'orchid',
    'palegoldenrod',
    'palegreen',
    'paleturquoise',
    'palevioletred',
    'papayawhip',
    'peachpuff',
    'peru',
    'pink',
    'plum',
    'powderblue',
    'rosybrown',
    'royalblue',
    'saddlebrown',
    'salmon',
    'sandybrown',
    'seagreen',
    'seashell',
    'sienna',
    'skyblue',
    'slateblue',
    'slategray',
    'slategrey',
    'snow',
    'springgreen',
    'steelblue',
    'tan',
    'thistle',
    'tomato',
    'turquoise',
    'violet',
    'wheat',
    'whitesmoke',
    'yellowgreen',
    'rebeccapurple',
]);

document.addEventListener('DOMContentLoaded', () => {
    var eyeses = Array(30).fill().map((_, i) => {
        return new Eyes({pupil: colours[i % colours.length]});
    });

    var id = undefined;
    function queue(mouse = {x: 0, y: 0}) {
        if (id === undefined) {
            id = window.requestAnimationFrame(() => {
                eyeses.forEach(eyes => eyes.draw(mouse));
                id = undefined;
            });
        }
    };

    queue();
    document.body.addEventListener('mousemove', (event) => {
        queue({
            x: event.clientX,
            y: event.clientY,
        });
    }, false);
});